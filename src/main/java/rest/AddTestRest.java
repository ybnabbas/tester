package rest;

import question.Question;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/add")
public class AddTestRest {

    @GET
    @Path("test")
    @Produces("application/json;charset=UTF-8")
    public Question add(){
        Question    question = new Question();
        question.setId(1);
        question.setQuestion("Hello");
        System.out.println(question);
        return question;
    }


}
