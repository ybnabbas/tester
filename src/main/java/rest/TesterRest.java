package rest;

import dao.QuestionService;
import question.Question;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

@Path("/get")
public class TesterRest {
    QuestionService questionService = new QuestionService();

    @Path("{id}")
    @GET
    @Produces("application/json;charset=UTF-8")
    public Question getQuestion(@PathParam("id") int id){
        return  questionService.getQuestion(id);
    }

//    @Path("/asdas")
//    @GET
//    @Produces("application/json;charset=UTF-8")
//    public List<Question> getQuestions(@PathParam("id") int id){
//       // return  questionService.getQuestion(id);
//    return null;
//    }

}
