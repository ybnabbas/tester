package dao;

import question.Question;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Ilyas on 18.07.2018.
 */
public class QuestionService {
    Connector connector = new Connector();
    Connection connection=connector.connect();
    Statement statement;
    ResultSet resultSet;
    public Question getQuestion(int id){
        Question question = new Question();
        try {
            statement=connection.createStatement();
            resultSet=statement.executeQuery("SELECT * from questions WHERE  id="+id+"");
            if (resultSet.next()){

                question.setQuestion(resultSet.getString("text"));
                question.setId(resultSet.getInt("id"));
            }
            return question;
        } catch (SQLException e) {
            e.printStackTrace();
            return  null;
        }

    }
}
