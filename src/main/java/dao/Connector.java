package dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Ilyas on 18.07.2018.
 */
public class Connector {
    Connection connection;
    InputStream inputStream;
    Properties properties = new Properties();
    public  Connection connect(){
        try {
            inputStream=Connector.class.getClassLoader().getResourceAsStream("db.properties");
            properties.load(inputStream);
            String url=properties.getProperty("db_url");
            String user=properties.getProperty("db_user");
            String password=properties.getProperty("db_password");
            if (connection==null){
                Class.forName(properties.getProperty("db_driver"));
                connection= DriverManager.getConnection(url,user,password);
            }
            return connection;
        }catch (SQLException | ClassNotFoundException | IOException e){
            e.printStackTrace();
            return null;
        }


    }
}
