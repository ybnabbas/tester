package authorize;


public class AuthorizeFactory {

    public AuthImpl create(int a){
        switch (a){
            case 1:
                return new Administrator();
            case 2:
                return new Organizer();
            case 3:
                return  new Student();
                default:
                    return null;
        }

    }
}
