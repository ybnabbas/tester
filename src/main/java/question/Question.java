package question;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Question {


    String question;
    int id;

    public Question() {
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Question{" +
                "question='" + question + '\'' +
                ", id=" + id +
                '}';
    }
}
